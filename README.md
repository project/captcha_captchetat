# CaptchEtat for Drupal

The "CaptchEtat (with CAPTCHA)" module uses the French government captcha solution to improve the CAPTCHA
system and protect form submissions.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/captchetat).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/captchetat).


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Requirements

This module requires the following modules:
- [CAPTCHA module](https://drupal.org/project/captcha)


## Configuration

1. Enable CaptchEtat and CAPTCHA modules in: *admin/modules*
2. You'll now find a CaptchEtat tab in the CAPTCHA administration page
   available at: *admin/config/people/captcha/captchetat*
3. [Request authorization (in French)](https://api.gouv.fr/les-api/api-captchetat/demande-acces)
   in order to consume the CaptchEtat APIs
4. Input the site and private keys into the CaptchEtat settings
5. Visit the Captcha administration page and set where you want the
   CaptchEtat form to be presented: *admin/config/people/captcha*

## References

* [CaptchEtat API (in French)](https://api.gouv.fr/les-api/api-captchetat)
* [CaptchEtat API documentation (in French)](https://api.gouv.fr/documentation/api-captchetat)
* [Request authorization (in French)](https://api.gouv.fr/les-api/api-captchetat/demande-acces)

## Maintainers

* Sébastien Brindle (S3b0uN3t) - https://www.drupal.org/user/2989133
