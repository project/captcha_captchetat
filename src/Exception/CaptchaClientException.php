<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Exception;

/**
 * Create custom class to identify captcha client exception.
 */
class CaptchaClientException extends \Exception {

}
