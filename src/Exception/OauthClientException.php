<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Exception;

/**
 * Create custom class to identify OAuth client exception.
 */
class OauthClientException extends \Exception {

}
