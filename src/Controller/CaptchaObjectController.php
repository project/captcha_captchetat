<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Controller;

use Drupal\captcha_captchetat\Client\CaptchaClientInterface;
use Drupal\captcha_captchetat\Helper\ClientHelperInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Flood\FloodInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Manage "CaptchEtat" captcha object callback.
 */
class CaptchaObjectController extends ControllerBase {

  /**
   * The flood captcha generate event name.
   *
   * @var string
   */
  const FLOOD_CAPTCHA_GENERATE = 'captcha_captchetat.captcha_generate';

  /**
   * The "CaptchEtat" client helper.
   *
   * @var \Drupal\captcha_captchetat\Helper\ClientHelperInterface
   */
  protected ClientHelperInterface $clientHelper;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected FloodInterface $flood;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->clientHelper = $container->get('captcha_captchetat.helper.client');
    $instance->flood = $container->get('flood');
    $instance->request = $container->get('request_stack')->getCurrentRequest();

    return $instance;
  }

  /**
   * Get the "CaptchEtat" object in query parameter.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The "CaptchEtat" object response.
   */
  public function get(): Response {
    $objectType = $this->request->query->get('get');
    $captchaId = $this->request->query->get('t');
    $captchaType = $this->request->query->get('c');

    // Captcha identifier is required for all requests.
    // Identifier is required for "sound" object type.
    if (
      empty($captchaType) ||
      (
        $objectType !== CaptchaClientInterface::OBJECT_TYPE_IMAGE &&
        empty($captchaId)
      )
    ) {
      throw new AccessDeniedHttpException();
    }

    $response = new Response();

    // In case of service is not available.
    if (!$this->clientHelper->isAvailable()) {
      return $this->getUnreachableResponse();
    }

    $config = $this->config('captcha_captchetat.settings');

    // Check if user flooding captcha generation.
    if (!$this->flood->isAllowed(
      self::FLOOD_CAPTCHA_GENERATE,
      $config->get('flood.ip_limit'),
      $config->get('flood.ip_window')
    )) {
      return $this->getFloodedResponse();
    }

    // Register user tentative in flood.
    $this->flood->register(self::FLOOD_CAPTCHA_GENERATE, $config->get('flood.ip_window'));

    $object = $this->clientHelper->getCaptcha(
      $objectType,
      $captchaType,
      $captchaId
    );

    // In case of service doesn't return captcha.
    if (empty($object)) {
      return $this->getUnreachableResponse();
    }

    $response->headers->set('Content-Type', 'application/json');
    $response->setContent($object);

    if (CaptchaClientInterface::OBJECT_TYPE_SOUND) {
      $response->headers->set(
        'Content-Disposition',
        'attachment; filename="' . $objectType . '.wav"'
      );
      $response->headers->set('Content-Type', 'audio/x-wav');
    }

    return $response;
  }

  /**
   * Get "CaptchEtat" flooded response.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The flooded AJAX response.
   */
  private function getFloodedResponse(): Response {
    // Log flood control denied.
    $this->getLogger('captcha_captchetat_flood_control')
      ->info('Captcha generation is blocked because of IP based flood prevention.');

    $response = new Response();
    $response->setStatusCode(429);
    $response->setContent(
      $this->config('captcha_captchetat.texts')->get('flood')
    );

    return $response;
  }


  /**
   * Get "CaptchEtat" unreachable response.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The unreachable response.
   */
  private function getUnreachableResponse(): Response {
    $response = new Response();
    $response->setStatusCode(503);
    $response->setContent(
      $this->config('captcha_captchetat.texts')->get('unreachable')
    );

    return $response;
  }


}
