<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Service;

/**
 * Provide a captcha service interface.
 */
interface CaptchaServiceInterface {

  /**
   * The "Captchetat" captcha type.
   *
   * @var string
   */
  const CAPTCHA_TYPE = 'CaptchEtat';

  /**
   * Generate "CaptchEtat" captcha widget.
   *
   * @return array
   *   A renderable array.
   */
  public function generate(): array;

  /**
   * Validate "CaptchEtat" captcha widget user input.
   *
   * @param string $code
   *   The input captcha code.
   *
   * @return bool
   *   True on success and false on failure.
   */
  public function validate(string $code): bool;

}
