<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Service;

use Drupal\captcha_captchetat\Client\CaptchaClientInterface;
use Drupal\captcha_captchetat\Helper\ClientHelperInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provide a captcha service.
 */
final class CaptchaService implements CaptchaServiceInterface {

  /**
   * The client helper.
   *
   * @var \Drupal\captcha_captchetat\Helper\ClientHelperInterface
   */
  protected ClientHelperInterface $clientHelper;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $request;

  /**
   * Constructs a "CaptchaService" object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\captcha_captchetat\Helper\ClientHelperInterface $client_helper
   *   The client helper.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RendererInterface $renderer,
    RequestStack $request_stack,
    ClientHelperInterface $client_helper
  ) {
    $this->clientHelper = $client_helper;
    $this->configFactory = $config_factory;
    $this->request = $request_stack->getCurrentRequest();
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.ElseExpression)
   */
  public function generate(): array {
    $captchaSettings = $this->configFactory->get('captcha_captchetat.settings');
    $captcha = [];

    // Build the "CaptchEtat" captcha form if OAuth required parameters are configured
    // and captcha API is reachable.
    if ($this->clientHelper->isAvailable()) {
      // Captcha requires TRUE to be returned in solution.
      $captcha['solution'] = TRUE;

      // As to validate callback does not depend on sid or solution, this
      // captcha type can be displayed on cached pages.
      $captcha['cacheable'] = TRUE;

      // Define captcha validation method.
      $captcha['captcha_validate'] = 'captcha_captchetat_captcha_validation';

      $widgetNoScript = ['#theme' => 'captcha_captchetat_widget_noscript'];

      $captcha['form']['captcha_widget'] = [
        '#type' => 'container',
        '#attributes' => [
          // The "CaptchEtat" solution needs this identifier.
          'id' => 'captchetat',
          'captchaStyleName' => $captchaSettings->get('widget.type'),
          'urlBackend' => Url::fromRoute('captcha_captchetat.object')->toString(),
        ],
        '#attached' => [
          'library' => ['captcha_captchetat/captchetat-js'],
        ],
      ];

      $captcha['form']['captcha_response'] = [
        '#type' => 'textfield',
        '#title' => t('CaptchEtat'),
        '#title_display' => 'invisible',
        '#required' => TRUE,
        '#maxlength' => 12,
        '#size' => 12,
        // The "CaptchEtat" solution needs this identifier on response input field.
        '#id' => 'captchaFormulaireExtInput',
        '#attributes' => [
          'autocomplete' => 'off',
          'required' => 'required',
        ],
        '#suffix' => $this->renderer->render($widgetNoScript),
      ];
    }
    else {
      // Fallback to default captcha as "CaptchEtat" is not configured or not reachable.
      $captcha = $this->generateDefault();
    }

    // If module configuration changes the form cache need to be refreshed.
    $this->renderer->addCacheableDependency(
      $captcha['form'],
      $captchaSettings
    );

    return $captcha;
  }

  /**
   * Generate default captcha.
   *
   * @return array
   *   A renderable array.
   */
  private function generateDefault(): array {
    $defaultChallenge = $this->configFactory->get('captcha.settings')
      ->get('default_challenge');

    if ($defaultChallenge !== 'captcha_captchetat/' . self::CAPTCHA_TYPE) {
      [$captchaModule, $captchaType] = explode('/', $defaultChallenge);
      $captchaCallback = $captchaModule . '_captcha';

      return $captchaCallback('generate', $captchaType);
    }

    return captcha_captcha('generate', 'Math');
  }

  /**
   * {@inheritdoc}
   */
  public function validate(string $code): bool {
    $captchaId = $this->request
      ->get('captchetat-uuid');

    if (empty($captchaId) || empty($code)) {
      return FALSE;
    }

    return $this->clientHelper->isCaptchaValid($captchaId, $code);
  }

}
