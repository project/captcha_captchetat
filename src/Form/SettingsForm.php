<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Form;

use Drupal\captcha_captchetat\Client\CaptchaClientInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides "CaptchEtat (with CAPTCHA)" settings form.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'captcha_captchetat_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['captcha_captchetat.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['general']['sandbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable sandbox mode'),
      '#description' => $this->t('Request the sandbox API instead of the production API.'),
      '#default_value' => $config->get('client.sandbox'),
    ];

    $form['general']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('The client identifier provided during authorization.'),
      '#default_value' => $config->get('client.oauth.client_id'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['general']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#description' => $this->t('The client secret provided during authorization.'),
      '#default_value' => $config->get('client.oauth.client_secret'),
      '#required' => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['general']['scope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scope'),
      '#description' => $this->t('The application API scope provided during authorization. The expected value is <strong>piste.captchetat</strong>.<br /><strong>Be sure to allow this scope in your application!</strong>'),
      '#default_value' => $config->get('client.oauth.scope'),
      '#required' => TRUE,
      '#disabled' => TRUE,
    ];

    $form['widget'] = [
      '#type' => 'details',
      '#title' => $this->t('Widget settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $optionsTypes = [];
    foreach (CaptchaClientInterface::TYPES as $type) {
      $optionsTypes[$type] = $type;
    }

    $form['widget']['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#description' => $this->t('The type of CAPTCHA to serve.'),
      '#default_value' => $config->get('widget.type'),
      '#options' => $optionsTypes,
      '#required' => TRUE,
    ];

    $form['widget']['type_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Available types'),
      '#open' => FALSE,
    ];

    $form['widget']['type_options']['table'] = $this->buildWidgetTypeTable();

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config($this->getEditableConfigNames()[0])
      ->set('client.sandbox', $form_state->getValue(['general', 'sandbox']))
      ->set('client.oauth.client_id', $form_state->getValue(['general', 'client_id']))
      ->set('client.oauth.client_secret', $form_state->getValue(['general', 'client_secret']))
      ->set('widget.type', $form_state->getValue(['widget', 'type']))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Build widget type table.
   *
   * @return array
   *   The renderable table array.
   */
  private function buildWidgetTypeTable(): array {
    return [
      '#theme' => 'table',
      '#header' => [
        $this->t('Difficulty level'),
        $this->t('Value'),
        $this->t('Description'),
      ],
      '#rows' => [
        [
          $this->t('Normal'),
          CaptchaClientInterface::TYPE_EN,
          $this->t('Captcha which returns an alphanumeric code between 6 and 9 characters.'),
        ],
        [
          $this->t('Normal'),
          CaptchaClientInterface::TYPE_FR,
          $this->t('Captcha which returns an alphanumeric code between 6 and 9 characters.'),
        ],
        [
          $this->t('Difficult'),
          CaptchaClientInterface::TYPE_ALPHABETIC_6_7_EN,
          $this->t('Captcha which returns an alphabetic code between 6 and 7 characters.'),
        ],
        [
          $this->t('Difficult'),
          CaptchaClientInterface::TYPE_ALPHABETIC_6_7_FR,
          $this->t('Captcha which returns an alphabetic code between 6 and 7 characters.'),
        ],
        [
          $this->t('Normal'),
          CaptchaClientInterface::TYPE_ALPHABETIC_12_EN,
          $this->t('Captcha which returns an alphabetic code of 12 characters.'),
        ],
        [
          $this->t('Normal'),
          CaptchaClientInterface::TYPE_ALPHABETIC_12_FR,
          $this->t('Captcha which returns an alphabetic code of 12 characters.'),
        ],
        [
          $this->t('Light'),
          CaptchaClientInterface::TYPE_ALPHANUMERIC_4_6_LIGHT_EN,
          $this->t('Captcha which returns an alphanumeric code between 4 and 6 characters.'),
        ],
        [
          $this->t('Light'),
          CaptchaClientInterface::TYPE_ALPHANUMERIC_4_6_LIGHT_FR,
          $this->t('Captcha which returns an alphanumeric code between 4 and 6 characters.'),
        ],
        [
          $this->t('Light'),
          CaptchaClientInterface::TYPE_ALPHANUMERIC_6_9_LIGHT_EN,
          $this->t('Captcha which returns an alphanumeric code between 6 and 9 characters.'),
        ],
        [
          $this->t('Light'),
          CaptchaClientInterface::TYPE_ALPHANUMERIC_6_9_LIGHT_FR,
          $this->t('Captcha which returns an alphanumeric code between 6 and 9 characters.'),
        ],
        [
          $this->t('Normal'),
          CaptchaClientInterface::TYPE_ALPHANUMERIC_12_EN,
          $this->t('Captcha which returns an alphanumeric code of 12 characters.'),
        ],
        [
          $this->t('Normal'),
          CaptchaClientInterface::TYPE_ALPHANUMERIC_12_EN,
          $this->t('Captcha which returns an alphanumeric code of 12 characters.'),
        ],
        [
          $this->t('Difficult'),
          CaptchaClientInterface::TYPE_NUMERICAL_6_7_EN,
          $this->t('Captcha which returns an numerical code between 6 and 7 characters.'),
        ],
        [
          $this->t('Difficult'),
          CaptchaClientInterface::TYPE_NUMERICAL_6_7_FR,
          $this->t('Captcha which returns an numerical code between 6 and 7 characters.'),
        ],
        [
          $this->t('Normal'),
          CaptchaClientInterface::TYPE_NUMERICAL_12_EN,
          $this->t('Captcha which returns an numerical code of 12 characters.'),
        ],
        [
          $this->t('Normal'),
          CaptchaClientInterface::TYPE_NUMERICAL_12_FR,
          $this->t('Captcha which returns an numerical code of 12 characters.'),
        ],
      ],
    ];
  }

}
