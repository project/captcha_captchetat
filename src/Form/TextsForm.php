<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides "CaptchEtat (with CAPTCHA)" texts form.
 */
final class TextsForm extends ConfigFormBase {

  /**
   * The text config keys.
   *
   * @var array
   */
  const KEYS = [
    'flood' => 'Too many tentatives',
    'unreachable' => 'Unreachable services',
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'captcha_captchetat_texts_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['captcha_captchetat.texts'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);

    foreach (self::KEYS as $key => $title) {
      $form[$key] = [
        '#type' => 'textarea',
        '#title' => $this->t($title),
        '#default_value' => $config->get($key),
        '#required' => TRUE,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);

    foreach (array_keys(self::KEYS) as &$key) {
      $config->set($key, $form_state->getValue($key));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
