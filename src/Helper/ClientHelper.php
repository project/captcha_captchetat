<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Helper;

use Drupal\captcha_captchetat\Client\CaptchaClientInterface;
use Drupal\captcha_captchetat\Exception\CaptchaClientException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Provide a client helper.
 */
final class ClientHelper implements ClientHelperInterface {

  /**
   * The "CaptchEtat" captcha client.
   *
   * @var \Drupal\captcha_captchetat\Client\CaptchaClientInterface
   */
  protected CaptchaClientInterface $client;

  /**
   * The "CaptchEtat" settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs a "ClientHelper" object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *    The config factory.
   * @param \Drupal\captcha_captchetat\Client\CaptchaClientInterface $captcha_client
   *   The Captcha client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *    A logger instance.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CaptchaClientInterface $captcha_client,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->client = $captcha_client;
    $this->config = $config_factory->get('captcha_captchetat.settings');
    $this->logger = $logger_factory->get('captcha_captchetat_client_helper');
  }

  /**
   * {@inheritdoc}
   */
  public function getCaptcha(
    string $objectType,
    string $captchaType,
    ?string $captchaId = NULL
  ): string {
    try {
      return $this->client->captcha($objectType, $captchaType, $captchaId) ?? '';
    }
    catch (CaptchaClientException $e) {
      $this->logger->warning(
        'Bad captcha parameter: %error.',
        ['%error' => $e->getMessage()]
      );
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getVersion(): string {
    return $this->client->version()['version'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function isAvailable(): bool {
    $data = $this->client->healthcheck();

    return (
      isset($data['status']) &&
      $data['status'] === 'UP'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isCaptchaValid(string $captchaId, string $code): bool {
    $data = $this->client->captchaValidate($captchaId, $code);

    return $data === 'true';
  }

}
