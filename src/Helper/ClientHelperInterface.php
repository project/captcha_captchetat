<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Helper;

/**
 * Provide a client helper interface.
 */
interface ClientHelperInterface {

  /**
   * Get Captcha client current version.
   *
   * @param string $objectType
   *   The object type to get.
   * @param string $captchaType
   *   The captcha type to get.
   * @param string|null $captchaId
   *   The captcha identifier (if captcha is already generated).
   *
   * @return string
   *   The captcha response.
   */
  public function getCaptcha(
    string $objectType,
    string $captchaType,
    ?string $captchaId = NULL
  ): string;

  /**
   * Get captcha client current version.
   *
   * @return string
   *   The client version.
   */
  public function getVersion(): string;

  /**
   * Check captcha client is available.
   *
   * @return bool
   *   True if the client is available, false otherwise.
   */
  public function isAvailable(): bool;

  /**
   * Check captcha input code for given identifier.
   *
   * @param string $captchaId
   *   The captcha identifier.
   * @param string $code
   *   The code input by the user.
   *
   * @return bool
   *   True if the code is valid, false otherwise.
   */
  public function isCaptchaValid(string $captchaId, string $code): bool;

}
