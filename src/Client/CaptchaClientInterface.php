<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Client;

use Drupal\captcha_captchetat\Exception\CaptchaClientException;

/**
 * Provide Captcha API client interface to request services.
 */
interface CaptchaClientInterface {

  /**
   * The generated captcha input contains identifier.
   * The input identifier is the concatenation of a prefix and the type of captcha generated.
   *
   * @var string
   */
  const CAPTCHA_INPUT_ID_PREFIX = 'BDC_VCID_';

  /**
   * Client base URLs.
   *
   * @var string
   */
  const BASE_URL_SANDBOX = 'https://sandbox-api.piste.gouv.fr/';
  const BASE_URL_PROD = 'https://api.piste.gouv.fr/';

  /**
   * Available object types.
   *
   * @var string
   */
  const OBJECT_TYPE_IMAGE = 'image';
  const OBJECT_TYPE_SOUND = 'sound';

  /**
   * List available object types.
   *
   * @var string[]
   */
  const OBJECT_TYPES = [
    self::OBJECT_TYPE_IMAGE,
    self::OBJECT_TYPE_SOUND,
  ];

  /**
   * Available captcha types.
   *
   * @var string
   */
  const TYPE_EN = 'captchaEN';
  const TYPE_FR = 'captchaFR';
  const TYPE_ALPHABETIC_6_7_EN = 'alphabetique6_7CaptchaEN';
  const TYPE_ALPHABETIC_6_7_FR = 'alphabetique6_7CaptchaFR';
  const TYPE_ALPHABETIC_12_EN = 'alphabetique12CaptchaEN';
  const TYPE_ALPHABETIC_12_FR = 'alphabetique12CaptchaFR';
  const TYPE_ALPHANUMERIC_4_6_LIGHT_EN = 'alphanumerique4to6LightCaptchaEN';
  const TYPE_ALPHANUMERIC_4_6_LIGHT_FR = 'alphanumerique4to6LightCaptchaFR';
  const TYPE_ALPHANUMERIC_6_9_LIGHT_EN = 'alphanumerique6to9LightCaptchaEN';
  const TYPE_ALPHANUMERIC_6_9_LIGHT_FR = 'alphanumerique6to9LightCaptchaFR';
  const TYPE_ALPHANUMERIC_12_EN = 'alphanumerique12CaptchaEN';
  const TYPE_ALPHANUMERIC_12_FR = 'alphanumerique12CaptchaFR';
  const TYPE_NUMERICAL_6_7_EN = 'numerique6_7CaptchaEN';
  const TYPE_NUMERICAL_6_7_FR = 'numerique6_7CaptchaFR';
  const TYPE_NUMERICAL_12_EN = 'numerique12CaptchaEN';
  const TYPE_NUMERICAL_12_FR = 'numerique12CaptchaFR';

  /**
   * List available captcha types.
   *
   * @var string[]
   */
  const TYPES = [
    self::TYPE_EN,
    self::TYPE_FR,
    self::TYPE_ALPHABETIC_6_7_EN,
    self::TYPE_ALPHABETIC_6_7_FR,
    self::TYPE_ALPHABETIC_12_EN,
    self::TYPE_ALPHABETIC_12_FR,
    self::TYPE_ALPHANUMERIC_4_6_LIGHT_EN,
    self::TYPE_ALPHANUMERIC_4_6_LIGHT_FR,
    self::TYPE_ALPHANUMERIC_6_9_LIGHT_EN,
    self::TYPE_ALPHANUMERIC_6_9_LIGHT_FR,
    self::TYPE_ALPHANUMERIC_12_EN,
    self::TYPE_ALPHANUMERIC_12_FR,
    self::TYPE_NUMERICAL_6_7_EN,
    self::TYPE_NUMERICAL_6_7_FR,
    self::TYPE_NUMERICAL_12_EN,
    self::TYPE_NUMERICAL_12_FR,
  ];

  /**
   * Request the captcha service.
   *
   * @param string $objectType
   *   The object type to get.
   * @param string $captchaType
   *   The captcha type to get.
   * @param string|null $captchaId
   *   The captcha identifier (if captcha is already generated).
   *
   * @return string|null
   *    The service response or null if request failed.
   *
   * @throws \Drupal\captcha_captchetat\Exception\CaptchaClientException
   */
  public function captcha(
    string $objectType,
    string $captchaType,
    ?string $captchaId = NULL
  ): ?string;

  /**
   * Request the captcha info service.
   *
   * @param string $captchaId
   *   The captcha identifier.
   *
   * @return array|null
   *   The service response or null if request failed.
   */
  public function captchaInfo(string $captchaId): ?array;

  /**
   * Request the captcha validation service.
   *
   * @param string $captchaId
   *   The captcha identifier.
   * @param string $code
   *   The code input by the user.
   *
   * @return string|null
   *   The service response or null if request failed.
   */
  public function captchaValidate(
    string $captchaId,
    string $code
  ): ?string;

  /**
   * Request the healthcheck service.
   *
   * @return array|null
   *   The service response or null if request failed.
   */
  public function healthcheck(): ?array;

  /**
   * Request the info service.
   *
   * @return array|null
   *   The service response or null if request failed.
   */
  public function info(): ?array;

  /**
   * Request the version service.
   *
   * @return array|null
   *   The service response or null if request failed.
   */
  public function version(): ?array;

}
