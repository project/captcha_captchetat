<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Client;

use Drupal\captcha_captchetat\Exception\OauthClientException;

/**
 * Provide OAuth client to request services.
 */
final class OauthClient extends ClientBase implements OauthClientInterface {

  /**
   * {@inheritdoc}
   */
  protected function getBaseUrl(): string {
    $baseUrl = self::BASE_URL;

    // Sandbox mode is enabled.
    if ($this->config->get('client.sandbox')) {
      $baseUrl = self::BASE_URL_SANDBOX;
    }

    return $baseUrl . 'api/oauth/';
  }

  /**
   * {@inheritdoc}
   */
  protected function getCachePrefix(): string {
    return 'captcha_captchetat:client:oauth:';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOptions(): array {
    return array_merge_recursive(parent::getDefaultOptions(), [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function authorize(): ?string {
    $cid = $this->getCachePrefix() . 'authorize';

    $cache = $this->cache->get($cid);
    if ($cache) {
      return $cache->data;
    }

    $response = $this->token();

    if (empty($response)) {
      return NULL;
    }

    $requiredProperties = ['access_token', 'expires_in', 'token_type'];
    foreach ($requiredProperties as &$property) {
      if (!isset($response[$property])) {
        throw new OauthClientException(
          sprintf('The property "%s" is missing.', $property)
        );
      }
    }

    $authorize = sprintf(
      '%s %s',
      $response['token_type'],
      $response['access_token']
    );

    $this->cache->set(
      $cid,
      $authorize,
      time() + $response['expires_in'],
      $this->config->getCacheTags()
    );

    return $authorize;
  }

  /**
   * {@inheritdoc}
   */
  public function token(): ?array {
    return $this->post('token', [
      'form_params' => [
        'client_id' => $this->config->get('client.oauth.client_id'),
        'client_secret' => $this->config->get('client.oauth.client_secret'),
        'grant_type' => 'client_credentials',
        'scope' => $this->config->get('client.oauth.scope'),
      ],
    ]);
  }

}
