<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Client;

/**
 * Provide client interface to request services.
 */
interface ClientInterface {

  /**
   * Client managed response format.
   *
   * @var string
   */
  const FORMAT_JSON = 'json';
  const FORMAT_PLAIN = 'plain';

}
