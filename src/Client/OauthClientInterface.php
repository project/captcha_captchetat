<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Client;

/**
 * Provide OAuth client interface to request services.
 */
interface OauthClientInterface {

  /**
   * Client base URLs.
   *
   * @var string
   */
  const BASE_URL = 'https://oauth.piste.gouv.fr/';
  const BASE_URL_SANDBOX = 'https://sandbox-oauth.piste.gouv.fr/';

  /**
   * Get OAuth authorization (concatenate "token_type" + "access_token").
   *
   * @return string|null
   *   The OAuth authorization or null if token request failed.
   *
   * @throws \Drupal\captcha_captchetat\Exception\OauthClientException
   */
  public function authorize(): ?string;

  /**
   * Get OAuth token.
   *
   * @return array|null
   *   The service response or null if request failed.
   */
  public function token(): ?array;

}
