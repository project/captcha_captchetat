<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Client;

use Drupal\captcha_captchetat\Exception\CaptchaClientException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\RequestOptions as GuzzleRequestOptions;

/**
 * Provide Captcha API client to request services.
 */
final class CaptchaClient extends ClientBase implements CaptchaClientInterface {

  /**
   * The "CaptchEtat" OAuth client.
   *
   * @var \Drupal\captcha_captchetat\Client\OauthClientInterface
   */
  protected OauthClientInterface $oauthClient;

  /**
   * Constructs a "CaptchaClient" object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The Drupal cache backend.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client used to do "CaptchEtat" API request.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger instance.
   * @param \Drupal\captcha_captchetat\Client\OauthClientInterface $oauth_client
   *   The "CaptchEtat" OAuth client.
   */
  public function __construct(
    CacheBackendInterface $cache,
    ConfigFactoryInterface $config_factory,
    GuzzleClientInterface $http_client,
    LoggerChannelFactoryInterface $logger_factory,
    OauthClientInterface $oauth_client
  ) {
    parent::__construct($cache, $config_factory, $http_client, $logger_factory);

    $this->oauthClient = $oauth_client;
  }

  /**
   * {@inheritdoc}
   */
  public function captcha(
    string $objectType,
    string $captchaType,
    ?string $captchaId = NULL
  ): ?string {
    // Check if given captcha object type exists.
    if (!in_array($objectType, self::OBJECT_TYPES)) {
      throw new CaptchaClientException(sprintf(
        'The object "%s" is not managed. Thanks to use available object types.',
        $objectType
      ));
    }

    // Check if given captcha type exists.
    if (!in_array($captchaType, self::TYPES)) {
      throw new CaptchaClientException(sprintf(
        'The type "%s" is not managed. Thanks to use available types.',
        $captchaType
      ));
    }

    $params = [
      'query' => [
        'c' => $captchaType,
        'get' => $objectType,
      ],
    ];

    if (isset($captchaId)) {
      $params['query']['t'] = $captchaId;
    }

    return $this->getWithoutCache(
      'simple-captcha-endpoint',
      $params,
      // Return response asis to be processed by CaptchEtat plugin.
      self::FORMAT_PLAIN
    );
  }

  /**
   * {@inheritdoc}
   */
  public function captchaInfo(string $captchaId): ?array {
    return $this->getWithoutCache("captcha/$captchaId/code/infos");
  }

  /**
   * {@inheritdoc}
   */
  public function captchaValidate(
    string $captchaId,
    string $code
  ): ?string {
    return $this->post(
      'valider-captcha',
      [
        GuzzleRequestOptions::JSON => [
          'uuid' => $captchaId,
          'code' => $code,
        ],
      ],
      self::FORMAT_PLAIN
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getBaseUrl(): string {
    $baseUrl = self::BASE_URL_PROD;

    // Sandbox mode is enabled.
    if ($this->config->get('client.sandbox')) {
      $baseUrl = self::BASE_URL_SANDBOX;
    }

    return $baseUrl . 'piste/captchetat/v2/';
  }

  /**
   * {@inheritdoc}
   */
  protected function getCachePrefix(): string {
    return 'captcha_captchetat:client:captcha:';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOptions(): array {
    return array_merge_recursive(parent::getDefaultOptions(), [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $this->oauthClient->authorize(),
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function healthcheck(): ?array {
    return $this->getWithoutCache('healthcheck');
  }

  /**
   * {@inheritdoc}
   */
  public function info(): ?array {
    // Caching response during 1 day.
    return $this->get('info', [], self::FORMAT_JSON, 86400);
  }

  /**
   * {@inheritdoc}
   */
  public function version(): ?array {
    // Caching response during 1 day.
    return $this->get('version', [], self::FORMAT_PLAIN, 86400);
  }

}
