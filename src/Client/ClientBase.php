<?php

declare(strict_types=1);

namespace Drupal\captcha_captchetat\Client;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provide an API client base to request services.
 */
abstract class ClientBase implements ClientInterface {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The "CaptchEtat" settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The HTTP client to fetch the embed code with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected GuzzleClientInterface $httpClient;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs a client base object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The Drupal cache backend.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client used to do "CaptchEtat" API request.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   A logger instance.
   */
  public function __construct(
    CacheBackendInterface $cache,
    ConfigFactoryInterface $config_factory,
    GuzzleClientInterface $http_client,
    LoggerChannelFactoryInterface $logger_factory
  ) {

    $this->cache = $cache;
    $this->config = $config_factory->get('captcha_captchetat.settings');
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('captcha_captchetat_client');
  }

  /**
   * Get theclient cache prefix.
   *
   * @return string
   *   The client cache prefix.
   */
  abstract protected function getCachePrefix(): string;

  /**
   * Get request default options.
   *
   * @return array
   *   The default options.
   */
  protected function getDefaultOptions(): array {
    return [
      'headers' => [
        'Accept' => 'application/json',
        'Charset' => 'utf-8',
      ],
    ];
  }

  /**
   * Client GET API request (with cache).
   *
   * @param string $service
   *   The client service to request.
   * @param array $params
   *   The client service parameters.
   * @param string $format
   *   The expected response format.
   * @param int $expires
   *   The expires duration (in seconds).
   *
   * @return array|string|null
   *   The response data or null if request failed.
   */
  protected function get(
    string $service,
    array $params = [],
    string $format = self::FORMAT_JSON,
    int $expires = 60
  ): array|string|null {
    $cid = $this->getCachePrefix() . $service . ':' . $format . ':' . md5(serialize($params));

    $cache = $this->cache->get($cid);

    if ($cache) {
      return $cache->data;
    }

    $response = $this->getWithoutCache($service, $params);

    // Don't cache response when error occurs.
    if (isset($response)) {
      $this->cache->set(
        $cid,
        $response,
        time() + $expires,
        $this->config->getCacheTags()
      );
    }

    return $response;
  }

  /**
   * Get the client base URL.
   *
   * @return string
   *   The client base URL.
   */
  abstract protected function getBaseUrl(): string;

  /**
   * Client GET API request (without cache).
   *
   * @param string $service
   *   The client service to request.
   * @param array $params
   *   The client service parameters.
   * @param string $format
   *   The expected response format.
   *
   * @return array|string|null
   *   The response data or null if request failed.
   */
  protected function getWithoutCache(
    string $service,
    array $params = [],
    string $format = self::FORMAT_JSON
  ): array|string|null {
    $response = $this->request(
      'GET',
      $service,
      $params
    );

    if (
      isset($response) &&
      $response->getStatusCode() === Response::HTTP_OK
    ) {
      $content = $response->getBody()->getContents();

      if ($format === self::FORMAT_JSON) {
        return Json::decode($content);
      }

      return $content;
    }

    return NULL;
  }

  /**
   * Client POST API request.
   *
   * @param string $service
   *   The client service to request.
   * @param array $params
   *   The client service parameters.
   * @param string $format
   *   The expected response format.
   *
   * @return array|string|null
   *   The request response or null if request failed.
   */
  protected function post(
    string $service,
    array $params,
    string $format = self::FORMAT_JSON
  ): array|string|null {
    $response = $this->request('POST', $service, $params);

    if (isset($response) &&
      in_array($response->getStatusCode(), [
        Response::HTTP_OK,
        Response::HTTP_CREATED,
        Response::HTTP_NO_CONTENT,
      ])
    ) {
      $content = $response->getBody()->getContents();

      if ($format === self::FORMAT_JSON) {
        return Json::decode($content);
      }

      return $content;
    }

    return NULL;
  }

  /**
   * Request the client service.
   *
   * @param string $method
   *   The HTTP request method.
   * @param string $service
   *   The client service to request.
   * @param array $options
   *   The client service options.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The client service response object, NULL is a problem thrown.
   */
  private function request(string $method, string $service, array $options): ?ResponseInterface {
    $url = $this->getBaseUrl() . $service;

    try {
      return $this->httpClient->request(
        $method,
        $url,
        array_merge_recursive($options, $this->getDefaultOptions())
      );
    }
    catch (GuzzleException $e) {
      $this->logger->warning(
        'Failed to request %url: %error.',
        [
          '%url' => $url,
          '%error' => $e->getMessage(),
        ]
      );
    }

    return NULL;
  }

}
