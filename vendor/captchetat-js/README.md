# Captchetat-JS Library

Ce projet est la bibliothèque en Javascript natif du Captchétat v2.

Elle permet d'intégrer le captchetat dans une application Javascript. Un guide d'implémentation est disponible sur le portail PISTE.

La CI du projet permet de compiler la bibliothèque via webpack puis de la pousser via npm publish sur le repository privé npm du nexus AIFE.